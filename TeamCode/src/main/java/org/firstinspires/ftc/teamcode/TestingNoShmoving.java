package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

@TeleOp
public class TestingNoShmoving extends LinearOpMode {
    public float red;
    public float green;
    public float blue;
    private final LibraryOfLies libraryOfLies = new LibraryOfLies();
    public void runOpMode() throws InterruptedException {
        libraryOfLies.initialize(hardwareMap, false);
        ElapsedTime updateTimer=new ElapsedTime();
        waitForStart();
        while (opModeIsActive()) {
            if(updateTimer.seconds()>1) {
                red=libraryOfLies.rightColorSensesRed();
                green=libraryOfLies.rightColorSensesGreen();
                blue=libraryOfLies.rightColorSensesBlue();
                telemetry.addData("Right Distance (in.)", libraryOfLies.rightDistanceSenses());
                telemetry.addData("Left Distance (in.)", libraryOfLies.leftDistanceSenses());
                if(red>green && red>blue){
                    telemetry.addData("COLOR VALUE","\n R: "+red+" +\n G: "+green+"\n B: "+blue);
                    telemetry.addData("Dominant Color", "RED");
                }
                else if(green>blue){
                    telemetry.addData("COLOR VALUE","\n R: "+red+"\n G: "+green+" +\n B: "+blue);
                    telemetry.addData("Dominant Color", "GREEN");
                }
                else{
                    telemetry.addData("COLOR VALUE","\n R: "+red+"\n G: "+green+"\n B: "+blue+" +");
                    telemetry.addData("Dominant Color", "BLUE");
                }
                telemetry.update();
                updateTimer.reset();
            }
        }
    }
}
