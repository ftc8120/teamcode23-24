package org.firstinspires.ftc.teamcode.teleop;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp

public class OdometryTest extends LinearOpMode {

    public void runOpMode() throws InterruptedException {

        DcMotor odometryPod = hardwareMap.get(DcMotor.class, "odo");
        waitForStart();
        while (opModeIsActive()) {
            int ticks = odometryPod.getCurrentPosition();
            telemetry.addData("Value", ticks);
            telemetry.update();

            // ~371 ticks per inch

        }
    }
}
