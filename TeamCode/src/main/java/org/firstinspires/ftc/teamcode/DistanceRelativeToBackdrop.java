package org.firstinspires.ftc.teamcode;

public enum DistanceRelativeToBackdrop {
    CLOSER,
    FARTHER,
}
