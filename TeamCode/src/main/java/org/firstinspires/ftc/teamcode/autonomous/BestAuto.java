package org.firstinspires.ftc.teamcode.autonomous;

import static org.firstinspires.ftc.teamcode.AllianceColor.BLUE;
import static org.firstinspires.ftc.teamcode.AllianceColor.RED;
import static org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop.CLOSER;
import static org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop.FARTHER;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.ftccommon.internal.manualcontrol.exceptions.FailedToOpenHubException;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.AllianceColor;
import org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop;
import org.firstinspires.ftc.teamcode.LibraryOfLies;
import org.firstinspires.ftc.vision.VisionPortal;
import org.firstinspires.ftc.vision.apriltag.AprilTagDetection;
import org.firstinspires.ftc.vision.apriltag.AprilTagProcessor;

import java.util.List;

@Autonomous
public class BestAuto extends LinearOpMode {
    public static final double FORWARD = Math.PI / 2;
    public static final double LEFT = Math.PI;
    public static final double RIGHT = 0;
    public static final double BACKWARD = -Math.PI / 2;
    private String prompt = "Red(B) or Blue(X)?";
    private String response = "";
    private int uiCounter = 0;
    AprilTagProcessor aprilTagProcessor;
    public double yaw = 0;
    public double offset = 0;
    public int tagID = 0;
    public int goal = 0;
    public AllianceColor allianceColor;
    public DistanceRelativeToBackdrop distanceRelativeToBackdrop;
    private final LibraryOfLies libraryOfLies = new LibraryOfLies();
    private final AprilFools aprilFools = new AprilFools();
    ElapsedTime aprilTagTimer;

    public double angleDif(double angle) {
        return angle - libraryOfLies.getHeading();
    }

    public void moveTile(double tileCount, double direction) throws InterruptedException {
        libraryOfLies.moveRobot(angleDif(direction), .50, 0);
        Thread.sleep(Math.round(1450 * tileCount));
        libraryOfLies.stopRobot();
    }

    @Override
    public void runOpMode() throws InterruptedException {
        libraryOfLies.initialize(hardwareMap, true);
        aprilTagProcessor = new AprilTagProcessor.Builder().build();
        VisionPortal visionPortal = VisionPortal.easyCreateWithDefaults(hardwareMap.get(WebcamName.class, "Webcam 1"), aprilTagProcessor);
        libraryOfLies.openSticks();
        while (!isStarted()) {
            telemetry.addData(prompt, response);
            telemetry.update();
            if (uiCounter == 0) {
                if (gamepad1.x) {
                    response += "BLUE ";
                    allianceColor = BLUE;
                    prompt = "Closer(A) or Farther(Y) :3";
                    uiCounter++;
                } else if (gamepad1.b) {
                    response += "RED ";
                    allianceColor = RED;
                    prompt = "Closer(A) or Farther(Y) :3";
                    uiCounter++;
                }
            } else if (uiCounter == 1) {
                if (gamepad1.a) {
                    response += "CLOSER ";
                    prompt = "READY TO GO X3";
                    distanceRelativeToBackdrop = CLOSER;
                    uiCounter++;
                } else if (gamepad1.y) {
                    response += "FARTHER ";
                    prompt = "READY TO GO X3";
                    distanceRelativeToBackdrop = FARTHER;
                    uiCounter++;
                }
            }
        }
        Thread.sleep(1500);
        libraryOfLies.customArm(.15);
        //libraryOfLies.moveElevatorTo(200);
        moveTile(1.05, FORWARD);
        libraryOfLies.stopRobot();
        Thread.sleep(100);
        boolean leftPropDetected = false;
        boolean rightPropDetected = false;
        ElapsedTime propDetectionTimer = new ElapsedTime();
        libraryOfLies.moveRobot(angleDif(FORWARD), .2, 0);
        while (propDetectionTimer.seconds() < 1.5) {
            leftPropDetected = leftPropDetected || libraryOfLies.isPropCloseOnLeft();
            rightPropDetected = rightPropDetected || libraryOfLies.isPropCloseOnRight();
        }
        libraryOfLies.stopRobot();
        telemetry.addData("SEE LEFT", leftPropDetected);
        telemetry.addData("SEE RIGHT", rightPropDetected);
        telemetry.addData("right distance", libraryOfLies.rightDistanceSenses());
        telemetry.addData("left distance", libraryOfLies.leftDistanceSenses());
        telemetry.update();
        Thread.sleep(200);
        if(leftPropDetected){
            goal = 1;
        } else if(rightPropDetected){
            goal = 3;
        }else{
            goal = 2;
        }
        if ((leftPropDetected && allianceColor == RED) || (rightPropDetected && allianceColor == BLUE)) { //LEFT RED, RIGHT BLUE
            backTurnAfterLeftRightProp();
            libraryOfLies.moveRobot(BACKWARD, 1, 0);
            Thread.sleep(550);
            libraryOfLies.quickStop();
            depositGroundPixel(300);
            if (distanceRelativeToBackdrop == CLOSER) {
                halfRotation();
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(850);
                libraryOfLies.quickStop();
                if(allianceColor == RED){
                    libraryOfLies.moveRobot(LEFT, .25, 0); //SLIGHT MOVE LEFT FOR BACKDROP, MUST CHANGE VALUES MAYBE TEST TEST TEST
                } else{
                    libraryOfLies.moveRobot(RIGHT, .25, 0); //SLIGHT MOVE LEFT FOR BACKDROP, MUST CHANGE VALUES MAYBE TEST TEST TEST

                }
                Thread.sleep(800);
                libraryOfLies.quickStop();
                placeBackdropPixel(visionPortal,0);
                moveAfterBackdropAndPark(1150);
            } else{
                libraryOfLies.quickStop();
                libraryOfLies.rotateToTarget(0);
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(600);
                libraryOfLies.quickStop();
                if (allianceColor == RED) {
                    libraryOfLies.rotateToTarget(-Math.PI / 2);
                } else {
                    libraryOfLies.rotateToTarget(Math.PI / 2);
                }
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(BACKWARD, 1, 0);
                Thread.sleep(1300);
                libraryOfLies.quickStop();
                halfRotation(.03);
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(1400);
                libraryOfLies.moveRobot(angleDif(BACKWARD), .5, 0);
                Thread.sleep(1650);
                libraryOfLies.quickStop();
                placeBackdropPixel(visionPortal,400);
            }

        } else if ((rightPropDetected && allianceColor == RED) || (leftPropDetected && allianceColor == BLUE)) { //RIGHT RED, LEFT BLUE

            backTurnAfterLeftRightProp();
            if(distanceRelativeToBackdrop==CLOSER){
                libraryOfLies.moveRobot(BACKWARD, .5, 0);
                Thread.sleep(1200);
            } else{
                libraryOfLies.moveRobot(FORWARD, .5, 0);
                Thread.sleep(200);
            }
            libraryOfLies.quickStop();
            depositGroundPixel(500);
            if (distanceRelativeToBackdrop == CLOSER) {
                halfRotation();
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(350); //WAY LESS BECAUSE CLOSER TO BACKDROP AFTER DROPPING PIXEL
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(angleDif(BACKWARD), .25, 0); //SLIGHT MOVE RIGHT FOR BACKDROP, MUST CHANGE VALUES MAYBE TEST TEST TEST
                Thread.sleep(1000);
                libraryOfLies.quickStop();
                placeBackdropPixel(visionPortal,0);
                moveAfterBackdropAndPark(850);
            } else {
                libraryOfLies.moveRobot(BACKWARD, .5, 0);
                Thread.sleep(300);
                libraryOfLies.quickStop();
                libraryOfLies.rotateToTarget(0);
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(600);
                libraryOfLies.quickStop();
                if (allianceColor == RED) {
                    libraryOfLies.rotateToTarget(-Math.PI / 2);
                } else {
                    libraryOfLies.rotateToTarget(Math.PI / 2);
                }
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(BACKWARD, .5, 0);
                Thread.sleep(1400);
                libraryOfLies.quickStop();
                halfRotation(.03);
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(1215);
                libraryOfLies.moveRobot(angleDif(BACKWARD), .5, 0);
                Thread.sleep(1650);
                libraryOfLies.quickStop();
                placeBackdropPixel(visionPortal,0);
            }


        } else { //CENTER BOTH
            libraryOfLies.moveRobot(BACKWARD, .5, 0);
            Thread.sleep(400);

            if (distanceRelativeToBackdrop == CLOSER) {
                depositGroundPixel(200);
                if (allianceColor == RED) {
                    libraryOfLies.rotateToTarget(Math.PI / 2);
                } else {
                    libraryOfLies.rotateToTarget(-Math.PI / 2);
                }
                libraryOfLies.moveRobot(FORWARD, .5, 0);
                Thread.sleep(2000);
                libraryOfLies.quickStop();
                placeBackdropPixel(visionPortal,0);
                moveAfterBackdropAndPark(850);
            } else { //FAR SIDE
                libraryOfLies.moveRobot(FORWARD, .5, 0);
                Thread.sleep(200);
                halfRotation();
                libraryOfLies.moveRobot(BACKWARD, .5, 0);
                Thread.sleep(600);
                depositGroundPixel(200);
                libraryOfLies.moveRobot(BACKWARD, .5, 0);
                Thread.sleep(500);
                libraryOfLies.quickStop();
                if (allianceColor == RED) {
                    libraryOfLies.rotateToTarget(-Math.PI / 2);
                } else {
                    libraryOfLies.rotateToTarget(Math.PI / 2);
                }
//                libraryOfLies.moveRobot(LEFT,.5,0);
//                Thread.sleep(200);
                libraryOfLies.moveRobot(BACKWARD, .5, 0);
                Thread.sleep(2600);
                libraryOfLies.quickStop();
                halfRotation(.03);
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(FORWARD, 1, 0);
                Thread.sleep(1090);
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(angleDif(BACKWARD), .5, 0);
                Thread.sleep(1425);
                libraryOfLies.quickStop();
                placeBackdropPixel(visionPortal,0);
            }
        }
        libraryOfLies.moveElevatorTo(0);
        Thread.sleep(1400);
        libraryOfLies.closeSticks();
        libraryOfLies.stopRobot();
    }

    private void halfRotation() throws InterruptedException {
        libraryOfLies.rotateToTarget(-libraryOfLies.getHeading() + Math.PI); //Negative makes counterclockwise angle into clockwise angle
    }

    private void halfRotation(double driftCorrectionValue) throws InterruptedException {
        libraryOfLies.rotateToTarget(-libraryOfLies.getHeading() + Math.PI + driftCorrectionValue);
    }

    private void placeBackdropPixel(VisionPortal visionPortal, long farther) throws InterruptedException {
        if(distanceRelativeToBackdrop== FARTHER){
            aprilTagTimer = new ElapsedTime();
            telemetryAprilTag();
            if (yaw != 0) {
                while (opModeIsActive() && (aprilTagTimer.seconds()<3) && ((Math.abs(yaw) > 1) || ((Math.abs(offset) > 0.25)))) {
                    if ((Math.abs(yaw) > 1)) {
                        yawTurn();
                    } else if ((Math.abs(offset) > 0.25)) {
                        adjustOffset();
                    } else {
                        libraryOfLies.stopRobot();
                    }
                    telemetryAprilTag();
                }
            }
            finalAdjust();
        }
        libraryOfLies.moveRobot(FORWARD,.5,0);
        Thread.sleep(farther);
        libraryOfLies.stopRobot();
        libraryOfLies.moveElevatorTo(1100);
        Thread.sleep(2000);
        libraryOfLies.midArm();
        Thread.sleep(500);
        libraryOfLies.moveRobot(FORWARD, .25, 0);
        Thread.sleep(325);
        libraryOfLies.quickStop();
        libraryOfLies.closeSticks();
        Thread.sleep(500);
        libraryOfLies.moveRobot(BACKWARD, .25, 0);
        Thread.sleep(280);
        libraryOfLies.quickStop();
        if(distanceRelativeToBackdrop==FARTHER){
            visionPortal.close();

        }
    }

    private void depositGroundPixel(int millis) throws InterruptedException {
        libraryOfLies.reverseIntake(.5);
        libraryOfLies.moveRobot(BACKWARD, .5, 0);
        Thread.sleep(millis); //WHEN SCANNING ON RIGHT IT WAS ORIGINALLY 700, IF ISSUES HAPPEN THEN IT MIGHT BE BECAUSE OF THIS
        libraryOfLies.stopIntake();
        libraryOfLies.quickStop();
    }

    private void moveAfterBackdropAndPark(int millis) throws InterruptedException {
        libraryOfLies.moveElevatorTo(0);
        libraryOfLies.lowArm();
        libraryOfLies.quickStop();
        libraryOfLies.moveRobot(angleDif(BACKWARD), 1, 0); //MOVE ROBOT TO PARK, SHOULD PROBABLY CHANGE MILLIS
        Thread.sleep(millis);
        libraryOfLies.quickStop();
        libraryOfLies.moveRobot(FORWARD, 1, 0);
        Thread.sleep(100);
        libraryOfLies.stopRobot();
    }

    private void backTurnAfterLeftRightProp() throws InterruptedException {
        libraryOfLies.moveRobot(angleDif(BACKWARD), .5, 0);
        Thread.sleep(400);
        libraryOfLies.quickStop();
        if (allianceColor == RED && distanceRelativeToBackdrop==CLOSER) {
            libraryOfLies.rotateToTarget(-Math.PI / 2);
        } else if (allianceColor == BLUE && distanceRelativeToBackdrop==CLOSER){
            libraryOfLies.rotateToTarget(Math.PI / 2);
        } else if (allianceColor == RED && distanceRelativeToBackdrop==FARTHER){
            libraryOfLies.rotateToTarget(Math.PI / 2);
        } else if (allianceColor == BLUE && distanceRelativeToBackdrop==FARTHER){
            libraryOfLies.rotateToTarget(-Math.PI / 2);
        }
        libraryOfLies.quickStop(); //MAKE SURE TO BE LINED UP WITH CENTER TAG OF BACKDROP WHEN THIS FUNCTION IS OVER
    }


    private void telemetryAprilTag() {
        List<AprilTagDetection> currentDetections = aprilTagProcessor.getDetections();
        telemetry.addData("# AprilTags Detected", currentDetections.size());
        double zero = 0;
        yaw = zero;
        // Step through the list of detections and display info for each one.
        for (AprilTagDetection detection : currentDetections) {
            if (detection.metadata != null) {
                telemetry.addLine(String.format("\n==== (ID %d) %s", detection.id, detection.metadata.name));
                telemetry.addLine(String.format("XYZ %6.1f %6.1f %6.1f  (inch)", detection.ftcPose.x, detection.ftcPose.y, detection.ftcPose.z));
                telemetry.addLine(String.format("PRY %6.1f %6.1f %6.1f  (deg)", detection.ftcPose.pitch, detection.ftcPose.roll, detection.ftcPose.yaw));
                telemetry.addLine(String.format("RBE %6.1f %6.1f %6.1f  (inch, deg, deg)", detection.ftcPose.range, detection.ftcPose.bearing, detection.ftcPose.elevation));
                yaw = detection.ftcPose.yaw;
                offset = detection.ftcPose.x;
            } else { //Do we need this??????
                telemetry.addLine(String.format("\n==== (ID %d) Unknown", detection.id));
                telemetry.addLine(String.format("Center %6.0f %6.0f   (pixels)", detection.center.x, detection.center.y));
            }
        }   // end for() loop

        // Add "key" information to telemetry
        telemetry.addLine("\nkey:\nXYZ = X (Right), Y (Forward), Z (Up) dist.");
        telemetry.addLine("PRY = Pitch, Roll & Yaw (XYZ Rotation)");
        telemetry.addLine("RBE = Range, Bearing & Elevation");
        telemetry.update();
    }

    public void yawTurn() {
        telemetryAprilTag();
        if ((Math.abs(yaw) > 1)) {
            libraryOfLies.moveRobot(0, 0, -yaw / 180);
        } else {
            libraryOfLies.stopRobot();
        }
    }
    public void adjustOffset() {
        telemetryAprilTag();
        telemetry.update();
        if(offset>0){
            libraryOfLies.moveRobot(RIGHT,.1,0);
        } else if (offset<0){
            libraryOfLies.moveRobot(LEFT,.1,0);
        } else {
            libraryOfLies.stopRobot();

        }
    }

    public void finalAdjust() throws InterruptedException {
        secondTelemetryAprilTag();
        if(tagID == 1 || tagID == 4){
            libraryOfLies.moveRobot(RIGHT,.5,0);
            if(goal == 2){
                Thread.sleep(300);
            } else if(goal == 3){
                Thread.sleep(600);
            }
            libraryOfLies.stopRobot();
        } else if(tagID == 2 || tagID == 5){
            if(goal == 1){
                libraryOfLies.moveRobot(LEFT,.5,0);
                Thread.sleep(300);
            } else if(goal == 3){
                libraryOfLies.moveRobot(RIGHT,.5,0);
                Thread.sleep(300);
            }
            libraryOfLies.stopRobot();
        } else if(tagID == 3 || tagID == 6){
            libraryOfLies.moveRobot(LEFT,.5,0);
            if(goal == 2){
                Thread.sleep(300);
            } else if(goal == 1){
                Thread.sleep(600);
            }
            libraryOfLies.stopRobot();
        }
    }
    private void secondTelemetryAprilTag() {
        List<AprilTagDetection> currentDetections = aprilTagProcessor.getDetections();
        telemetry.addData("# AprilTags Detected", currentDetections.size());
        // Step through the list of detections and display info for each one.
        for (AprilTagDetection detection : currentDetections) {
            if (detection.metadata != null) {
                telemetry.addLine(String.format("\n==== (ID %d) %s", detection.id, detection.metadata.name));
                telemetry.addLine(String.format("XYZ %6.1f %6.1f %6.1f  (inch)", detection.ftcPose.x, detection.ftcPose.y, detection.ftcPose.z));
                telemetry.addLine(String.format("PRY %6.1f %6.1f %6.1f  (deg)", detection.ftcPose.pitch, detection.ftcPose.roll, detection.ftcPose.yaw));
                telemetry.addLine(String.format("RBE %6.1f %6.1f %6.1f  (inch, deg, deg)", detection.ftcPose.range, detection.ftcPose.bearing, detection.ftcPose.elevation));
                tagID = detection.id;
            } else {
                telemetry.addLine(String.format("\n==== (ID %d) Unknown", detection.id));
                telemetry.addLine(String.format("Center %6.0f %6.0f   (pixels)", detection.center.x, detection.center.y));
            }
        }   // end for() loop

        // Add "key" information to telemetry
        telemetry.addLine("\nkey:\nXYZ = X (Right), Y (Forward), Z (Up) dist.");
        telemetry.addLine("PRY = Pitch, Roll & Yaw (XYZ Rotation)");
        telemetry.addLine("RBE = Range, Bearing & Elevation");
    }
//Possibly use all three april tag yaw values to line up to the board

//Find camera's position in relation to three april tags, draw line between tags to establish the plane, line robot up parallel to the plane


}











































//Stop using only time values, validate movements based on real world measurements (Summer Project??!!??)