package org.firstinspires.ftc.teamcode.autonomous;

import static org.firstinspires.ftc.teamcode.AllianceColor.BLUE;
import static org.firstinspires.ftc.teamcode.AllianceColor.RED;
import static org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop.CLOSER;
import static org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop.FARTHER;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.AllianceColor;
import org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop;
import org.firstinspires.ftc.teamcode.LibraryOfLies;

@Autonomous
public class OldAutoCode extends LinearOpMode {
    public static final double FORWARD = Math.PI / 2;
    public static final double LEFT = Math.PI;
    public static final double RIGHT = 0;
    public static final double BACKWARD = -Math.PI / 2;
    private String prompt="Red(B) or Blue(X)?";
    private String response="";
    private int uiCounter=0;
    public AllianceColor allianceColor;
    public DistanceRelativeToBackdrop distanceRelativeToBackdrop;
    private final LibraryOfLies libraryOfLies = new LibraryOfLies();
    public double angleDif(double angle){
        return angle - libraryOfLies.getHeading();
    }
    public void moveTile(double tileCount, double direction) throws InterruptedException {
        libraryOfLies.moveRobot(angleDif(direction), .50, 0);
        Thread.sleep(Math.round(1450 * tileCount));
        libraryOfLies.stopRobot();
    }

    @Override
    public void runOpMode() throws InterruptedException {
        libraryOfLies.initialize(hardwareMap, true);
        libraryOfLies.closeSticks();
        while(!isStarted()){
            telemetry.addData(prompt, response);
            telemetry.update();
            if(uiCounter==0) {
                if (gamepad1.x) {
                    response += "BLUE ";
                    allianceColor = BLUE;
                    prompt = "Closer(A) or Farther(Y)";
                    uiCounter++;
                } else if (gamepad1.b) {
                    response += "RED ";
                    allianceColor = RED;
                    prompt = "Closer(A) or Farther(Y)";
                    uiCounter++;
                }
            }
            else if(uiCounter==1) {
                if (gamepad1.a) {
                    response += "CLOSER ";
                    prompt = "READY TO GO";
                    distanceRelativeToBackdrop = CLOSER;
                    uiCounter++;
                } else if (gamepad1.y) {
                    response += "FARTHER ";
                    prompt = "READY TO GO";
                    distanceRelativeToBackdrop = FARTHER;
                    uiCounter++;
                }
            }
        }
        libraryOfLies.openSticks();
        Thread.sleep(100);
        libraryOfLies.moveElevatorTo(200);
        moveTile(1.05,FORWARD);
        libraryOfLies.stopRobot();
        Thread.sleep(100);
        boolean leftPropDetected = false;
        boolean rightPropDetected = false;
        ElapsedTime propDetectionTimer = new ElapsedTime();
        libraryOfLies.moveRobot(FORWARD, .2, 0);
        while(propDetectionTimer.seconds()<1.5){
            leftPropDetected = leftPropDetected || libraryOfLies.isPropCloseOnLeft();
            rightPropDetected = rightPropDetected || libraryOfLies.isPropCloseOnRight();
        }
        libraryOfLies.stopRobot();
        telemetry.addData("SEE LEFT", leftPropDetected);
        telemetry.addData("SEE RIGHT", rightPropDetected);
        telemetry.addData("right distance",libraryOfLies.rightDistanceSenses());
        telemetry.addData("left distance",libraryOfLies.leftDistanceSenses());
        telemetry.update();
        Thread.sleep(200);
        if(leftPropDetected){
            backTurnAfterLeftRightProp();
            libraryOfLies.moveRobot(FORWARD,.5,0);
            Thread.sleep(250);
            libraryOfLies.quickStop();
            depositGroundPixel(600);
            if(distanceRelativeToBackdrop == CLOSER){
                halfRotation();
                libraryOfLies.moveRobot(FORWARD,1,0);
                Thread.sleep(750);
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(LEFT,.25,0); //SLIGHT MOVE LEFT FOR BACKDROP, MUST CHANGE VALUES MAYBE TEST TEST TEST
                Thread.sleep(400);
                libraryOfLies.quickStop();
                placeBackdropPixel();
                moveAfterBackdropAndPark(500);
            }

        }else if(rightPropDetected){
            backTurnAfterLeftRightProp();
            libraryOfLies.moveRobot(BACKWARD,.5,0);
            Thread.sleep(1200);
            libraryOfLies.quickStop();
            depositGroundPixel(600);
            if(distanceRelativeToBackdrop == CLOSER){
                halfRotation();
                libraryOfLies.moveRobot(FORWARD,1,0);
                Thread.sleep(250); //WAY LESS BECAUSE CLOSER TO BACKDROP AFTER DROPPING PIXEL
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(RIGHT,.25,0); //SLIGHT MOVE RIGHT FOR BACKDROP, MUST CHANGE VALUES MAYBE TEST TEST TEST
                Thread.sleep(400);
                libraryOfLies.quickStop();
                placeBackdropPixel();
                moveAfterBackdropAndPark(300);
            }


        }else{
            libraryOfLies.moveRobot(BACKWARD,.5,0);
            Thread.sleep(400);
            depositGroundPixel(200);
            if(distanceRelativeToBackdrop == CLOSER){
                libraryOfLies.moveRobot(BACKWARD,1,0);
                Thread.sleep(300);
                libraryOfLies.moveRobot(BACKWARD,0,.5); //ROTATE ROBOT 90 DEG, MILLISECONDS OF TURN SHOULD WORK MAYBE
                Thread.sleep(800);
                libraryOfLies.quickStop();
                libraryOfLies.moveRobot(FORWARD,1,0);
                Thread.sleep(350); //SOME LESS BECAUSE CLOSER TO BACKDROP AFTER DROPPING PIXEL FOR CENTER
                libraryOfLies.quickStop();
                placeBackdropPixel();
                moveAfterBackdropAndPark(500);
            }
        }
        libraryOfLies.stopRobot();

    }

    private void halfRotation() throws InterruptedException {
        libraryOfLies.moveRobot(BACKWARD,0,-.5); //ROTATE ROBOT
        Thread.sleep(1400);
        libraryOfLies.quickStop();
    }

    private void placeBackdropPixel() throws InterruptedException {
        libraryOfLies.moveElevatorTo(1100);
        Thread.sleep(2000);
        libraryOfLies.midArm();
        Thread.sleep(500);
        libraryOfLies.moveRobot(FORWARD,.25,0);
        Thread.sleep(400);
        libraryOfLies.quickStop();
        libraryOfLies.closeSticks();
        Thread.sleep(500);
        libraryOfLies.moveRobot(BACKWARD,.25,0);
        Thread.sleep(200);
        libraryOfLies.quickStop();
    }

    private void depositGroundPixel(int millis) throws InterruptedException {
        libraryOfLies.reverseIntake(.5);
        libraryOfLies.moveRobot(BACKWARD,.5,0);
        Thread.sleep(millis); //WHEN SCANNING ON RIGHT IT WAS ORIGINALLY 700, IF ISSUES HAPPEN THEN IT MIGHT BE BECAUSE OF THIS
        libraryOfLies.stopIntake();
    }

    private void moveAfterBackdropAndPark(int millis) throws InterruptedException {
        libraryOfLies.moveRobot(RIGHT,1,0); //MOVE ROBOT TO PARK, SHOULD PROBABLY CHANGE MILLIS
        Thread.sleep(millis);
        libraryOfLies.quickStop();
        libraryOfLies.moveRobot(FORWARD,1,0);
        Thread.sleep(200);
        libraryOfLies.stopRobot();
    }

    private void backTurnAfterLeftRightProp() throws InterruptedException {
        libraryOfLies.moveRobot(BACKWARD,.5,0);
        Thread.sleep(400);
        libraryOfLies.quickStop();
        libraryOfLies.moveRobot(0,0,-.5);
        Thread.sleep(800);
        libraryOfLies.quickStop(); //MAKE SURE TO BE LINED UP WITH CENTER TAG OF BACKDROP WHEN THIS FUNCTION IS OVER
    }
}