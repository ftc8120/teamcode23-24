package org.firstinspires.ftc.teamcode.autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.LibraryOfLies;
import org.firstinspires.ftc.vision.VisionPortal;
import org.firstinspires.ftc.vision.apriltag.AprilTagDetection;
import org.firstinspires.ftc.vision.apriltag.AprilTagProcessor;

import java.util.List;
@Autonomous
public class AprilFools extends LinearOpMode {
    public double YAW = 0;
    public double offset = 0;
    public int tagID = 0;
    public static final double LEFT = Math.PI;
    public static final double RIGHT = 0;
    private static final LibraryOfLies LIBRARY_OF_LIES = new LibraryOfLies();
    AprilTagProcessor aprilTagProcessor;


    @Override
    public void runOpMode() throws InterruptedException {
        LIBRARY_OF_LIES.initialize(hardwareMap,true);
        aprilTagProcessor = new AprilTagProcessor.Builder().build();


        VisionPortal visionPortal = VisionPortal.easyCreateWithDefaults(hardwareMap.get(WebcamName.class, "Webcam 1"), aprilTagProcessor);

        waitForStart();
        while(opModeIsActive()) {
            telemetryAprilTag();
            telemetry.update();
            if (YAW != 1928374657438.2819) {
                while (((Math.abs(YAW) > 1) || ((Math.abs(offset) > 0.15))) && YAW!=1928374657438.2819) {
                    if ((Math.abs(YAW) > 1)) {
                            yawTurn();
                    } else if ((Math.abs(offset) > 0.25)) {
                        adjustOffset();
                    } else {
                        LIBRARY_OF_LIES.stopRobot();
                    }
                    telemetryAprilTag();
                }
            } else {
                LIBRARY_OF_LIES.moveRobot(0, 0, .01);
            }
//            if(YAW!=0){
//                LIBRARY_OF_LIES.moveRobot(YAW,1,0);
//            }
        }
        visionPortal.close();
    }


    private void telemetryAprilTag() {
        List<AprilTagDetection> currentDetections = aprilTagProcessor.getDetections();
        telemetry.addData("# AprilTags Detected", currentDetections.size());
        double zero=1928374657438.2819;
        YAW = zero;
        // Step through the list of detections and display info for each one.
        for (AprilTagDetection detection : currentDetections) {
            if (detection.metadata != null) {
                telemetry.addLine(String.format("\n==== (ID %d) %s", detection.id, detection.metadata.name));
                telemetry.addLine(String.format("XYZ %6.1f %6.1f %6.1f  (inch)", detection.ftcPose.x, detection.ftcPose.y, detection.ftcPose.z));
                telemetry.addLine(String.format("PRY %6.1f %6.1f %6.1f  (deg)", detection.ftcPose.pitch, detection.ftcPose.roll, detection.ftcPose.yaw));
                telemetry.addLine(String.format("RBE %6.1f %6.1f %6.1f  (inch, deg, deg)", detection.ftcPose.range, detection.ftcPose.bearing, detection.ftcPose.elevation));
                YAW = detection.ftcPose.yaw;
                offset = detection.ftcPose.x;
            } else {
                telemetry.addLine(String.format("\n==== (ID %d) Unknown", detection.id));
                telemetry.addLine(String.format("Center %6.0f %6.0f   (pixels)", detection.center.x, detection.center.y));
            }
        }   // end for() loop

        // Add "key" information to telemetry
        telemetry.addLine("\nkey:\nXYZ = X (Right), Y (Forward), Z (Up) dist.");
        telemetry.addLine("PRY = Pitch, Roll & Yaw (XYZ Rotation)");
        telemetry.addLine("RBE = Range, Bearing & Elevation");
    }
    public void yawTurn() {
        telemetryAprilTag();
        telemetry.update();
        if((Math.abs(YAW)>1)){
            LIBRARY_OF_LIES.moveRobot(0,0,-YAW/180);
        } else {
            LIBRARY_OF_LIES.stopRobot();
        }
    }
    public void adjustOffset() {
        telemetryAprilTag();
        telemetry.update();
        if(offset>0){
            LIBRARY_OF_LIES.moveRobot(RIGHT,.1,0);
        } else if (offset<0){
            LIBRARY_OF_LIES.moveRobot(LEFT,.1,0);
        } else {
            LIBRARY_OF_LIES.stopRobot();

        }
    }
//    private void secondTelemetryAprilTag() {
//        List<AprilTagDetection> currentDetections = aprilTagProcessor.getDetections();
//        telemetry.addData("# AprilTags Detected", currentDetections.size());
//        double zero=0;
//        YAW = zero;
//        // Step through the list of detections and display info for each one.
//        for (AprilTagDetection detection : currentDetections) {
//            if (detection.metadata != null) {
//                telemetry.addLine(String.format("\n==== (ID %d) %s", detection.id, detection.metadata.name));
//                telemetry.addLine(String.format("XYZ %6.1f %6.1f %6.1f  (inch)", detection.ftcPose.x, detection.ftcPose.y, detection.ftcPose.z));
//                telemetry.addLine(String.format("PRY %6.1f %6.1f %6.1f  (deg)", detection.ftcPose.pitch, detection.ftcPose.roll, detection.ftcPose.yaw));
//                telemetry.addLine(String.format("RBE %6.1f %6.1f %6.1f  (inch, deg, deg)", detection.ftcPose.range, detection.ftcPose.bearing, detection.ftcPose.elevation));
//                tagID = detection.id;
//            } else {
//                telemetry.addLine(String.format("\n==== (ID %d) Unknown", detection.id));
//                telemetry.addLine(String.format("Center %6.0f %6.0f   (pixels)", detection.center.x, detection.center.y));
//            }
//        }   // end for() loop
//
//        // Add "key" information to telemetry
//        telemetry.addLine("\nkey:\nXYZ = X (Right), Y (Forward), Z (Up) dist.");
//        telemetry.addLine("PRY = Pitch, Roll & Yaw (XYZ Rotation)");
//        telemetry.addLine("RBE = Range, Bearing & Elevation");
//    }
//    public void finalAdjust(int goal) throws InterruptedException {
//        secondTelemetryAprilTag();
//        if(tagID == 1 || tagID == 4){
//            LIBRARY_OF_LIES.moveRobot(RIGHT,.5,0);
//            if(goal == 2){
//               Thread.sleep(300);
//            } else if(goal == 3){
//                Thread.sleep(600);
//            }
//            LIBRARY_OF_LIES.stopRobot();
//        } else if(tagID == 2 || tagID == 5){
//            if(goal == 1){
//                LIBRARY_OF_LIES.moveRobot(LEFT,.5,0);
//                Thread.sleep(300);
//            } else if(goal == 3){
//                LIBRARY_OF_LIES.moveRobot(RIGHT,.5,0);
//                Thread.sleep(300);
//            }
//            LIBRARY_OF_LIES.stopRobot();
//        } else if(tagID == 3 || tagID == 6){
//            LIBRARY_OF_LIES.moveRobot(LEFT,.5,0);
//            if(goal == 2){
//                Thread.sleep(300);
//            } else if(goal == 1){
//                Thread.sleep(600);
//            }
//            LIBRARY_OF_LIES.stopRobot();
//        }
//    }
}
