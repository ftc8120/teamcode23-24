package org.firstinspires.ftc.teamcode;

import androidx.annotation.NonNull;

import com.qualcomm.hardware.rev.RevHubOrientationOnRobot;
import com.qualcomm.robotcore.hardware.ColorRangeSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.IMU;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

public class LibraryOfLies {
    DcMotor backLeft;
    DcMotor backRight;
    DcMotor frontLeft;
    DcMotor frontRight;
    ColorRangeSensor rightSensor;
    ColorRangeSensor leftSensor;
    DcMotor redNomNomSpinner;
    DcMotor elevator;
    Servo chopsticks;
    Servo arm;
    Servo airplane;
    DcMotor helper;

    IMU imu;

    RevHubOrientationOnRobot.LogoFacingDirection logoOrientation = RevHubOrientationOnRobot.LogoFacingDirection.LEFT;
    RevHubOrientationOnRobot.UsbFacingDirection usbOrientation = RevHubOrientationOnRobot.UsbFacingDirection.UP;

    private static final double HALF_TURN = Math.PI;
    private static final double FULL_TURN = 2 * HALF_TURN;

    public void initialize(@NonNull HardwareMap hardwareMap, boolean shouldResetYaw) {
        frontLeft = hardwareMap.get(DcMotor.class, "fl");
        frontRight = hardwareMap.get(DcMotor.class, "fr");
        backLeft = hardwareMap.get(DcMotor.class, "bl");
        backRight = hardwareMap.get(DcMotor.class, "br");
        imu = hardwareMap.get(IMU.class, "imu");
        rightSensor = hardwareMap.get(ColorRangeSensor.class, "rightSensor");
        leftSensor = hardwareMap.get(ColorRangeSensor.class, "leftSensor");
        elevator = hardwareMap.get(DcMotor.class, "elevator");
        redNomNomSpinner = hardwareMap.get(DcMotor.class, "spin");
        chopsticks = hardwareMap.get(Servo.class, "chopsticks");
        arm = hardwareMap.get(Servo.class, "arm");
        helper = hardwareMap.get(DcMotor.class, "helper");
        airplane = hardwareMap.get(Servo.class, "airplane");

        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction. I changed this line of code.
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction. I changed this line of code.
        redNomNomSpinner.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction. I changed this line of code.
        elevator.setDirection(DcMotorSimple.Direction.REVERSE); //Builders did something wrong, I end up having to fix it, such is life


        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        redNomNomSpinner.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        elevator.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        chopsticks.getController().pwmEnable();
        arm.getController().pwmEnable();
        airplane.setPosition(.7);
        airplane.getController().pwmEnable();
        helper.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        redNomNomSpinner.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        helper.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        setBottomOfElevator();

        initializeIMU();
        if(shouldResetYaw){
            imu.resetYaw();
        }
    }

    public void spatialCalculation() {
        imu.resetYaw();
    }

    public void moveRobot(double angleRelativeToRobot, double speed, double rightTurnPower) {
        double mecanumAdjustedAngle = angleRelativeToRobot - Math.toRadians(45); //Turn the angle 45 degrees to make things normal
        double forwardRightPower = Math.cos(mecanumAdjustedAngle);
        double forwardLeftPower = Math.sin(mecanumAdjustedAngle);
        double frontLeftPower = (((speed * forwardRightPower) + rightTurnPower)); //makes wheels move correctly
        double frontRightPower = (((speed * forwardLeftPower) - rightTurnPower)); //makes wheels move correctly
        double backLeftPower = (((speed * forwardLeftPower) + rightTurnPower)); //makes wheels move correctly
        double backRightPower = (((speed * forwardRightPower) - rightTurnPower)); //makes wheels move correctly
        double greater = Math.max(Math.abs(frontRightPower), Math.abs(backLeftPower));
        double high = Math.max(Math.abs(backRightPower), Math.abs(frontLeftPower));
        double powerdivisor = Math.max(greater, high);

        if (powerdivisor <= 1) {
            powerdivisor = 1;
        }

        frontRight.setPower(frontRightPower / powerdivisor);
        frontLeft.setPower(frontLeftPower / powerdivisor);
        backLeft.setPower(backLeftPower / powerdivisor);
        backRight.setPower(backRightPower / powerdivisor);

        //Right Turn power controls spinning
        //(speed*forward___Power) controls movement
    }

    public void initializeIMU(){
        IMU.Parameters parameters = new IMU.Parameters(new RevHubOrientationOnRobot(logoOrientation, usbOrientation));
        imu.initialize(parameters);
    }

    public double getHeading() {
        return imu.getRobotYawPitchRollAngles().getYaw(AngleUnit.RADIANS); //COUNTER CLOCKWISE (VERIFIED)
    }

    public void stopRobot() {
        frontLeft.setPower(0);
        frontRight.setPower(0);
        backLeft.setPower(0);
        backRight.setPower(0);
    }

    public void zeroElevator(){
        DcMotor.RunMode standardMode=elevator.getMode();
        elevator.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        elevator.setMode(standardMode);
    }

    public void closeSticks(){chopsticks.setPosition(0);}
    public void openSticks(){chopsticks.setPosition(1);}

    public void midArm(){arm.setPosition(.52);}
    public void lowArm(){arm.setPosition(0.222);}
    public void backArm(){arm.setPosition(.15);}
    public void customArm(double pos){arm.setPosition(pos);}

    public double getArmPosition(){
        return arm.getPosition();
    }

    public void turnToAngle(double targetAngle){
        double difOfAngle = targetAngle + getHeading();
        if (difOfAngle > HALF_TURN) {
            difOfAngle -= FULL_TURN;
        }
        if (difOfAngle < -HALF_TURN) {
            difOfAngle += FULL_TURN;
        }
        moveRobot(HALF_TURN/2, 0, calcRightTurn(difOfAngle));
    }

    /**
     * Rotate to a target heading angle
     * @param targetHeadingAngle The angle to turn the robot to, but it needs to be clockwise. Very IMPORTANT
     */
    public void rotateToTarget(double targetHeadingAngle) {
        double rightTurnPower;
        double difOfAngle;
        do {
            difOfAngle = targetHeadingAngle + getHeading();
            if (difOfAngle > HALF_TURN) {
                difOfAngle -= FULL_TURN;
            }
            if (difOfAngle < -HALF_TURN) {
                difOfAngle += FULL_TURN;
            }
            rightTurnPower = calcRightTurn(difOfAngle);
            moveRobot(0, 0, rightTurnPower);
        } while (rightTurnPower != 0);
    }

    public void stopIntake() {
        redNomNomSpinner.setPower(0);
    }

    public void startIntake() {
        redNomNomSpinner.setPower(.75);
    }

    public void reverseIntake() {
        redNomNomSpinner.setPower(-.75);
    }

    public void stopHelper() {
        helper.setPower(0);
    }

    public void launchDrone() {
        airplane.setPosition(1);
    }

    public void resetDroneServo(){
        airplane.setPosition(0.7);
    }

    public void startHelper() {
        helper.setPower(.5);
    }

    public void reverseHelper() {
        helper.setPower(-.5);
    }

    public void onlyHelperMoveIn() {
        helper.setPower(1);
    }
    public void onlyHelperMoveOut() {
        helper.setPower(-1);
    }



    public void quickStop() throws InterruptedException{
        stopRobot();
        Thread.sleep(200);
    }

    public void reverseIntake(double power) {
        redNomNomSpinner.setPower(-power);
    }

    public double calcRightTurn(double difOfAngle) {

        double power = difOfAngle / Math.PI;
        if (power <= .0025 && power >= -.0025) {
            return 0;
        }

        return (difOfAngle / Math.PI) * 1.5;
    }

    public double rightDistanceSenses(){
        return rightSensor.getDistance(DistanceUnit.INCH);
    }

    public double leftDistanceSenses(){
        return leftSensor.getDistance(DistanceUnit.INCH);
    }

    public float rightColorSensesRed(){
        return rightSensor.getNormalizedColors().red;
    }

    public float rightColorSensesBlue(){
        return rightSensor.getNormalizedColors().blue;
    }

    public float rightColorSensesGreen(){
        return rightSensor.getNormalizedColors().green;
    }

    public boolean isPropCloseOnRight(){
        double threshold=2;
        return rightDistanceSenses()<threshold;
    }

    public boolean isPropCloseOnLeft() {
        double threshold = 1.7;
        return leftDistanceSenses() < threshold;
    }

    public void setBottomOfElevator(){
        elevator.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        elevator.setTargetPosition(0); //Sets the 0 elevator position
        elevator.setMode(DcMotor.RunMode.RUN_TO_POSITION);
    }

    public void stopElevator(){
        elevator.setPower(0); //Stops the elevator
    }

    public void moveElevatorTo(int position){
        elevator.setTargetPosition(position);
        elevator.setPower(.8);
    }

    public void slowLiftElevator(){
        elevator.setTargetPosition(10000);
        elevator.setPower(.25);
    }

    public void slowDropElevator(){
        elevator.setTargetPosition(-10000);
        elevator.setPower(.25);
    }

    public int checkElevatorPosition(){
        return elevator.getCurrentPosition();
    }

    public int getElevatorTargetPosition(){
        return elevator.getTargetPosition();
    }

    public double getStickPosition(){
        return chopsticks.getPosition();
    }

    public void liftElevator(){
        elevator.setTargetPosition(10000);
        elevator.setPower(1);
    }

    public void dropElevator(){
        elevator.setTargetPosition(-10000);
        elevator.setPower(-1);
    }


}

