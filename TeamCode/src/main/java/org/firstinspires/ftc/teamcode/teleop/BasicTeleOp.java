package org.firstinspires.ftc.teamcode.teleop;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.LibraryOfLies;

@TeleOp
public class BasicTeleOp extends LinearOpMode {
    private static final double HALF_TURN = Math.PI;
    private static final double FULL_TURN = 2 * HALF_TURN;
    private final LibraryOfLies libraryOfLies = new LibraryOfLies();
    private int fixedElevatorPosition=0;
    private boolean chopHeld=false;
    private boolean xHeld=false;
    private boolean yHeld=false;
    private boolean armBackLow=false;
    private boolean autoArmPosition=false;
    private double customArmPos=.223;

    public void runOpMode() throws InterruptedException {
        libraryOfLies.initialize(hardwareMap, false);
        waitForStart();
        while (opModeIsActive()) {
            double leftStickAngle = Math.atan2(-gamepad1.left_stick_y, gamepad1.left_stick_x); //finds the COUNTER CLOCKWISE angle in radians between the stick and the x axis
            double speed = Math.hypot(-gamepad1.left_stick_y, gamepad1.left_stick_x); //distance of stick from (0,0)
            double gyroAngle = leftStickAngle - libraryOfLies.getHeading(); //sets the angle ______ to the driver
            telemetry.addData("VALUE",gamepad2.left_stick_y);
            telemetry.addData("Elevator Position",libraryOfLies.checkElevatorPosition());
            telemetry.addData("CHOP POS",libraryOfLies.getStickPosition());
            telemetry.addData("ARM POS",libraryOfLies.getArmPosition());

            //RIGHT STICK MOVEMENT

            double rightTurnPower = 0;
            if (gamepad1.right_stick_x != 0 || gamepad1.right_stick_y != 0) {
                double rightStickAngle = Math.atan2(gamepad1.right_stick_x, -gamepad1.right_stick_y); //CLOCKWISE
                double difOfAngle = rightStickAngle + libraryOfLies.getHeading(); //I changed this line of code.
                if (difOfAngle > HALF_TURN) {
                    difOfAngle -= FULL_TURN;
                }
                if (difOfAngle < -HALF_TURN) {
                    difOfAngle += FULL_TURN;
                }
                rightTurnPower = libraryOfLies.calcRightTurn(difOfAngle); //RightTurnPower is set equal to the output of the function calcRightTurn given input "difOfAngle"
            }
            libraryOfLies.moveRobot(gyroAngle, speed, rightTurnPower);
            telemetry.addData("Speed", speed);
            telemetry.update();

            if (gamepad1.left_bumper && gamepad1.right_bumper && gamepad1.x) {
                libraryOfLies.spatialCalculation();  //Reorients the robot according to its new rotation
            }

            if (gamepad1.left_trigger != 0) {
                libraryOfLies.reverseIntake();
                libraryOfLies.reverseHelper();
            } else if (gamepad1.right_trigger != 0) {
                libraryOfLies.startIntake();
                libraryOfLies.startHelper();
            } else if (!xHeld && !yHeld){
                libraryOfLies.stopIntake();
            }

            if (gamepad1.x && !xHeld && !yHeld){
                libraryOfLies.onlyHelperMoveIn();
            }
            else if (gamepad1.y && !xHeld && !yHeld){
                libraryOfLies.onlyHelperMoveOut();
            }
            else if (!gamepad1.x && !gamepad1.y && gamepad1.right_trigger==0 && gamepad1.left_trigger==0){
                libraryOfLies.stopHelper();
            }
            xHeld=gamepad1.x;
            yHeld=gamepad1.y;

//            if (gamepad1.left_bumper && !leftBumperHeld){
//                libraryOfLies.reverseHelper();
//            }
//            else if (!gamepad1.left_bumper && gamepad1.right_trigger==0){
//                libraryOfLies.stopHelper();
//            }
//            leftBumperHeld=gamepad1.left_bumper;

            if(libraryOfLies.checkElevatorPosition()<=1000 && libraryOfLies.getStickPosition()==1 && libraryOfLies.checkElevatorPosition()>=350 && libraryOfLies.getArmPosition()!=customArmPos){
                if(!armBackLow){
                    armBackLow=true;
                }
                libraryOfLies.backArm();
            }
            else if(libraryOfLies.checkElevatorPosition()<=350 && libraryOfLies.getStickPosition()!=1 && libraryOfLies.checkElevatorPosition()>=100 && libraryOfLies.getArmPosition()!=customArmPos){
                armBackLow=false;
                libraryOfLies.lowArm();
            }
//            else if(libraryOfLies.checkElevatorPosition()<=350 && libraryOfLies.checkElevatorPosition()>=100 && libraryOfLies.getArmPosition()!=customArmPos){
//                armBackLow=false;
//                libraryOfLies.lowArm();
//            }
            else if(armBackLow && libraryOfLies.checkElevatorPosition()>1000 || armBackLow && libraryOfLies.checkElevatorPosition()<100){
                armBackLow=false;
                libraryOfLies.lowArm();
            }


            //GAMEPAD 2

            if(gamepad2.left_bumper && gamepad2.right_bumper && gamepad2.b){
                libraryOfLies.zeroElevator();
            }

            if(gamepad2.b && !chopHeld && !armBackLow && !gamepad2.left_bumper){
                if(libraryOfLies.getStickPosition()!=1) {
                    libraryOfLies.openSticks();
                }
                else {
                    libraryOfLies.closeSticks();
                }
            }
            chopHeld=gamepad2.b;


            if(gamepad2.a && !armBackLow){
                //libraryOfLies.lowArm();
                libraryOfLies.customArm(customArmPos);
            }
            if(gamepad2.x && !armBackLow){
                //libraryOfLies.midArm();
                libraryOfLies.customArm(.50);
            }
            if(gamepad2.y && !armBackLow){
                //libraryOfLies.midArm();
                libraryOfLies.customArm(.15);
            }


            if(gamepad2.left_bumper && gamepad2.y){
                libraryOfLies.moveElevatorTo(fixedElevatorPosition);
            }


            if(gamepad2.dpad_up){
                libraryOfLies.slowLiftElevator();
            }
            else if(gamepad2.dpad_down) {
                libraryOfLies.slowDropElevator();
            }
            else if(gamepad2.left_stick_y<0){
                libraryOfLies.liftElevator();
            }
            else if(gamepad2.left_stick_y>0){
                libraryOfLies.dropElevator();
            }
            else if(libraryOfLies.getElevatorTargetPosition()!=10000 || libraryOfLies.getElevatorTargetPosition()!=-10000){
                libraryOfLies.moveElevatorTo(libraryOfLies.checkElevatorPosition());
            }

            if((gamepad2.left_bumper) && (gamepad2.right_bumper) && (gamepad2.left_trigger !=0) && (gamepad2.right_trigger !=0)){
                libraryOfLies.launchDrone();
            }
            if(gamepad2.dpad_left){
                libraryOfLies.resetDroneServo();
            }
        }
    }
}
