package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;

@TeleOp
public class ColorDistanceSensorTests extends LinearOpMode {
    public String dist;
    private final LibraryOfLies libraryOfLies = new LibraryOfLies();
    public void runOpMode() throws InterruptedException {
        libraryOfLies.initialize(hardwareMap, false);
        waitForStart();
        while (opModeIsActive()) {
            telemetry.addData("Right Distance Sensor Reading", libraryOfLies.rightDistanceSenses());
            if(libraryOfLies.rightDistanceSenses()<2) {
                dist="CLOSE";
                telemetry.addData("COLOR VALUE:",libraryOfLies.rightColorSensesRed()+" "+libraryOfLies.rightColorSensesGreen()+" "+libraryOfLies.rightColorSensesBlue());
                telemetry.addData("Object", "AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH");
                libraryOfLies.stopRobot();
            }
            else{
                if(!dist.equals("FAR")) { //Complaining about not using equals() because it is comparing string values
                    dist = "FAR";
                    libraryOfLies.moveRobot(90, .50, 0);
                }
            }
            telemetry.addData("Right Distance Sensor Approximation", dist);
            telemetry.update();
        }
    }
}
