package org.firstinspires.ftc.teamcode.autonomous;

import static org.firstinspires.ftc.teamcode.AllianceColor.BLUE;
import static org.firstinspires.ftc.teamcode.AllianceColor.RED;
import static org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop.CLOSER;
import static org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop.FARTHER;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.AllianceColor;
import org.firstinspires.ftc.teamcode.DistanceRelativeToBackdrop;
import org.firstinspires.ftc.teamcode.LibraryOfLies;

@Autonomous
public class SlightBetterTrialAuto extends LinearOpMode {
    public static final double FORWARD = Math.PI / 2;
    public static final double LEFT = Math.PI;
    public static final double RIGHT = 0;
    public static final double BACKWARD = -Math.PI / 2;
    private String prompt="Red(B) or Blue(X)?";
    private String response="";
    private int uiCounter=0;
    private boolean extraScore=false;
    public AllianceColor allianceColor;
    public DistanceRelativeToBackdrop distanceRelativeToBackdrop;
    private final LibraryOfLies libraryOfLies = new LibraryOfLies();
    public double angleDif(double angle){
        return angle - libraryOfLies.getHeading();
    }
    public void moveTile(double tileCount, double direction) throws InterruptedException {
        libraryOfLies.moveRobot(angleDif(direction), .50, 0);
        Thread.sleep(Math.round(1450 * tileCount));
        libraryOfLies.stopRobot();
    }

    @Override
    public void runOpMode() throws InterruptedException {
        libraryOfLies.initialize(hardwareMap, true);
        while(!isStarted()){
            telemetry.addData(prompt, response);
            telemetry.update();
            if(uiCounter==0) {
                if (gamepad1.x) {
                    response += "BLUE ";
                    allianceColor = BLUE;
                    prompt = "Closer(A) or Farther(Y)";
                    uiCounter++;
                } else if (gamepad1.b) {
                    response += "RED ";
                    allianceColor = RED;
                    prompt = "Closer(A) or Farther(Y)";
                    uiCounter++;
                }
            }
            else if(uiCounter==1) {
                if (gamepad1.a) {
                    response += "CLOSER ";
                    prompt = "Score(X) or Park(B)";
                    distanceRelativeToBackdrop = CLOSER;
                    uiCounter++;
                } else if (gamepad1.y) {
                    response += "FARTHER ";
                    prompt = "Score(X) or Park(B)";
                    distanceRelativeToBackdrop = FARTHER;
                    uiCounter++;
                }
            }
            else if(uiCounter==2 && distanceRelativeToBackdrop==FARTHER){
                if (gamepad1.x) {
                    response += "SCORE";
                    extraScore=true;
                    prompt = "READY";
                    uiCounter++;
                } else if (gamepad1.b) {
                    response += "PARK";
                    extraScore=false;
                    prompt = "READy";
                    uiCounter++;
                }
            }
        }
        moveTile(1,FORWARD);
        if (distanceRelativeToBackdrop == CLOSER){
            moveTile(.125, FORWARD);
            moveTile(1,BACKWARD);
            if (allianceColor == RED){
                moveTile(2, RIGHT);
            }
            else{
                moveTile(2, LEFT);
            }
        }
        else if(extraScore){
            Thread.sleep(100);
            moveTile(.25,BACKWARD);
            if(allianceColor==BLUE) {
                moveTile(.5, RIGHT);
                libraryOfLies.turnToAngle(LEFT);
            }
            else{
                moveTile(.5, LEFT);
                libraryOfLies.turnToAngle(RIGHT);
            }
            moveTile(1.25,FORWARD);
        }
    }
}
