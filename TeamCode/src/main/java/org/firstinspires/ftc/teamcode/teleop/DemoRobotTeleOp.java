package org.firstinspires.ftc.teamcode.teleop;

import androidx.annotation.NonNull;

import com.qualcomm.hardware.rev.RevHubOrientationOnRobot;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.IMU;

import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.teamcode.LibraryOfLies;

@TeleOp
public class DemoRobotTeleOp extends LinearOpMode {
    private static final double HALF_TURN = Math.PI;
    private static final double FULL_TURN = 2 * HALF_TURN;
    DcMotor backLeft;
    DcMotor backRight;
    DcMotor frontLeft;
    DcMotor frontRight;
    IMU imu;

    RevHubOrientationOnRobot.LogoFacingDirection logoOrientation = RevHubOrientationOnRobot.LogoFacingDirection.LEFT;
    RevHubOrientationOnRobot.UsbFacingDirection usbOrientation = RevHubOrientationOnRobot.UsbFacingDirection.UP;

    public void runOpMode() throws InterruptedException {
        smallInitialize(hardwareMap, true);
        waitForStart();
        while (opModeIsActive()) {
            double leftStickAngle = Math.atan2(-gamepad1.left_stick_y, gamepad1.left_stick_x); //finds the COUNTER CLOCKWISE angle in radians between the stick and the x axis
            double speed = Math.hypot(-gamepad1.left_stick_y, gamepad1.left_stick_x); //distance of stick from (0,0)
            double gyroAngle = leftStickAngle - getHeading(); //sets the angle ______ to the driver

            //RIGHT STICK MOVEMENT

            double rightTurnPower = 0;
            if (gamepad1.right_stick_x != 0 || gamepad1.right_stick_y != 0) {
                double rightStickAngle = Math.atan2(gamepad1.right_stick_x, -gamepad1.right_stick_y); //CLOCKWISE
                double difOfAngle = rightStickAngle + getHeading(); //I changed this line of code.
                if (difOfAngle > HALF_TURN) {
                    difOfAngle -= FULL_TURN;
                }
                if (difOfAngle < -HALF_TURN) {
                    difOfAngle += FULL_TURN;
                }
                rightTurnPower = calcRightTurn(difOfAngle); //RightTurnPower is set equal to the output of the function calcRightTurn given input "difOfAngle"
            }
            moveRobot(gyroAngle, speed, rightTurnPower);
            telemetry.addData("Speed", speed);
            telemetry.update();

            if (gamepad1.left_bumper && gamepad1.right_bumper && gamepad1.x) {
                spatialCalculation();  //Reorients the robot according to its new rotation
            }
        }
    }

    public void smallInitialize(@NonNull HardwareMap hardwareMap, boolean shouldResetYaw) {
        frontLeft = hardwareMap.get(DcMotor.class, "fl");
        frontRight = hardwareMap.get(DcMotor.class, "fr");
        backLeft = hardwareMap.get(DcMotor.class, "bl");
        backRight = hardwareMap.get(DcMotor.class, "br");
        imu = hardwareMap.get(IMU.class, "imu");


        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction. I changed this line of code.
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE); //Flip motor direction. I changed this line of code.


        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        frontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);

        initializeIMU();
        if(shouldResetYaw){
            imu.resetYaw();
        }
    }
    public void initializeIMU(){
        IMU.Parameters parameters = new IMU.Parameters(new RevHubOrientationOnRobot(logoOrientation, usbOrientation));
        imu.initialize(parameters);
    }
    public void moveRobot(double angleRelativeToRobot, double speed, double rightTurnPower) {
        double mecanumAdjustedAngle = angleRelativeToRobot - Math.toRadians(45); //Turn the angle 45 degrees to make things normal
        double forwardRightPower = Math.cos(mecanumAdjustedAngle);
        double forwardLeftPower = Math.sin(mecanumAdjustedAngle);
        double frontLeftPower = (((speed * forwardRightPower) + rightTurnPower)); //makes wheels move correctly
        double frontRightPower = (((speed * forwardLeftPower) - rightTurnPower)); //makes wheels move correctly
        double backLeftPower = (((speed * forwardLeftPower) + rightTurnPower)); //makes wheels move correctly
        double backRightPower = (((speed * forwardRightPower) - rightTurnPower)); //makes wheels move correctly
        double greater = Math.max(Math.abs(frontRightPower), Math.abs(backLeftPower));
        double high = Math.max(Math.abs(backRightPower), Math.abs(frontLeftPower));
        double powerdivisor = Math.max(greater, high);

        if (powerdivisor <= 1) {
            powerdivisor = 1;
        }

        frontRight.setPower(frontRightPower / powerdivisor);
        frontLeft.setPower(frontLeftPower / powerdivisor);
        backLeft.setPower(backLeftPower / powerdivisor);
        backRight.setPower(backRightPower / powerdivisor);

        //Right Turn power controls spinning
        //(speed*forward___Power) controls movement
    }
    public double calcRightTurn(double difOfAngle) {

        double power = difOfAngle / Math.PI;
        if (power <= .0025 && power >= -.0025) {
            return 0;
        }

        return (difOfAngle / Math.PI) * 1.5;
    }
    public void spatialCalculation() {
        imu.resetYaw();
    }
    public double getHeading() {
        return imu.getRobotYawPitchRollAngles().getYaw(AngleUnit.RADIANS); //COUNTER CLOCKWISE (VERIFIED)
    }


}
