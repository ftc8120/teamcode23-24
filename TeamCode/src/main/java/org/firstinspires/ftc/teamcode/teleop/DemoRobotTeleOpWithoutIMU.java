package org.firstinspires.ftc.teamcode.teleop;

import androidx.annotation.NonNull;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

@TeleOp
public class DemoRobotTeleOpWithoutIMU extends LinearOpMode {
    private static final double HALF_TURN = Math.PI;
    private static final double FULL_TURN = 2 * HALF_TURN;
    DcMotor backLeft;
    DcMotor backRight;
    DcMotor frontLeft;
    DcMotor frontRight;

    public void runOpMode() throws InterruptedException {
        smallInitialize(hardwareMap);
        waitForStart();
        while (opModeIsActive()) {
            double gyroAngle = Math.atan2(-gamepad1.left_stick_y, gamepad1.left_stick_x);
            double speed = Math.hypot(-gamepad1.left_stick_y, gamepad1.left_stick_x);


            //RIGHT STICK MOVEMENT

            double rightTurnPower = 0;
            if (gamepad1.right_stick_x != 0 || gamepad1.right_stick_y != 0) {
                double difOfAngle = Math.atan2(gamepad1.right_stick_x, -gamepad1.right_stick_y);
                if (difOfAngle > HALF_TURN) {
                    difOfAngle -= FULL_TURN;
                }
                if (difOfAngle < -HALF_TURN) {
                    difOfAngle += FULL_TURN;
                }
                rightTurnPower = calcRightTurn(difOfAngle);
            }
            moveRobot(gyroAngle, speed, rightTurnPower);
            telemetry.addData("Speed", speed);
            telemetry.update();
        }
    }

    public void smallInitialize(@NonNull HardwareMap hardwareMap) {
        frontLeft = hardwareMap.get(DcMotor.class, "fl");
        frontRight = hardwareMap.get(DcMotor.class, "fr");
        backLeft = hardwareMap.get(DcMotor.class, "bl");
        backRight = hardwareMap.get(DcMotor.class, "br");


        frontLeft.setDirection(DcMotorSimple.Direction.REVERSE);
        backLeft.setDirection(DcMotorSimple.Direction.REVERSE);


        frontRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRight.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeft.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);

        frontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        frontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        backRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
    }

    public void moveRobot(double angleRelativeToRobot, double speed, double rightTurnPower) {
        double mecanumAdjustedAngle = angleRelativeToRobot - Math.toRadians(45);
        double forwardRightPower = Math.cos(mecanumAdjustedAngle);
        double forwardLeftPower = Math.sin(mecanumAdjustedAngle);
        double frontLeftPower = (((speed * forwardRightPower) + rightTurnPower));
        double frontRightPower = (((speed * forwardLeftPower) - rightTurnPower));
        double backLeftPower = (((speed * forwardLeftPower) + rightTurnPower));
        double backRightPower = (((speed * forwardRightPower) - rightTurnPower));
        double greater = Math.max(Math.abs(frontRightPower), Math.abs(backLeftPower));
        double high = Math.max(Math.abs(backRightPower), Math.abs(frontLeftPower));
        double powerdivisor = Math.max(greater, high);

        if (powerdivisor <= 1) {
            powerdivisor = 1;
        }


        frontRight.setPower(frontRightPower / powerdivisor);
        frontLeft.setPower(frontLeftPower / powerdivisor);
        backLeft.setPower(backLeftPower / powerdivisor);
        backRight.setPower(backRightPower / powerdivisor);

    }

    public double calcRightTurn(double difOfAngle) {

        double power = difOfAngle / Math.PI;
        if (power <= .0025 && power >= -.0025) {
            return 0;
        }

        return (difOfAngle / Math.PI) * 1.5;
    }
}
